# Persistent-Memory-Analysis-Tool-Full-Paper

Persistent Memory Analysis Tool (Full Paper)

After each commit, the paper will be built; see jobs here: 

https://gitlab.com/LouisJenkinsCS/persistent-memory-analysis-tool-full-paper/-/jobs

Then click on `artifact` to view `PMAT.pdf`; if there is a red 'X', it failed to build.
If there is a green check, it was successful and the artifact is ready to be viewed.
If it is blue, the repository is still being checked and requires a bit of additional time.